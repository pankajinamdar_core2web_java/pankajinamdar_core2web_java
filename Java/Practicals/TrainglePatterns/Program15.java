/*4.Write a program to print the given pattern
rows=3
c
C B
c b a
rows=4
d
D C
d c b
D C B A*/
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException {
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Row:");
	int row=Integer.parseInt(br.readLine());
	for(int i=1;i<=row;i++){
		int n11=row+64;
		int n2=row+96;
		for(int j=1;j<=i;j++){
			if(i%2==1){
				System.out.print((char)n2 +" " );
				n2--;
			}else{
				System.out.print((char)n11 +" " );
				n11--;
			}
		}
		System.out.println();
	
	}
	
	}

}
