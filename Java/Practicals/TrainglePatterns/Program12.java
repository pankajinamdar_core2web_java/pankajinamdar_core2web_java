/*
 1. Write a program to print the given pattern
rows=3
9
9 9
9 9 9*/


import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter Row:");
	int row=Integer.parseInt(br.readLine());
	for(int i=1;i<=row;i++){
		int num=row*row;
		for(int j=1;j<=i;j++){
			System.out.print(num +" ");
		}
		System.out.println();
	
	}
	
	}

}
