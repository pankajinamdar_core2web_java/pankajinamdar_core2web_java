import java.io.*;

class  Demo{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row ; i++){

			char ch1 = 'A';
			char ch2 = 'a';

			for(int j = 1; j<=row-i+1 ; j++){

				if(i%2==1){
				System.out.print(ch1++ + "\t");
				}else{
				System.out.print(ch2++ + "\t");
				}

			}

			System.out.println();

		}

	}

}
