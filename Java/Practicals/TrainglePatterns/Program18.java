/*7. Write a program to print the given pattern
rows=3
1
2 a
3 b 3

rows=4
1
2 a
3 b 3
4 c 4 d*/
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Row:");
	int row=Integer.parseInt(br.readLine());
	int num1=row+94;
	for(int i=1;i<=row;i++){
		int num=i;
		for(int j=1;j<=i;j++){
			if(j%2==1){
				System.out.print(num +" ");
			}else{
				System.out.print((char)num1 +" ");
				num1++;	
			}
			//num1++;
		}
		System.out.println();	
		}
	}
}
