/* 1 2 3 4
     1 2 3
       1 2
         1
         */
import java.util.*;
class Demo{
        public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.print("Rows:");
        int row=sc.nextInt();
        for(int i=1;i<=row;i++){
                for(int space=1;space<i;space++){
                        System.out.print(" ");
                }
                int num=1;
                for(int j=row;j>=i;j--){
                        System.out.print(num+"");
                        num++;
                }
                System.out.println();
        }
}
}
