/*3. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
       1
    2  4
  3 6  9
4 8 12 16 
     
     
     
     */
import java.util.*;
class Demo{
        public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.print("Row:");
        int row=sc.nextInt();
        int num=1;
        for(int i=1;i<=row;i++){
                for(int space=1;space<=row-i;space++){
                        System.out.print("  ");
                }
                for(int j=1;j<=i;j++){
                        System.out.print(num*j+" ");
                }
		num++;
		System.out.println();
		//num--;
		}
	}
}
