

class ques3b_C2W{

	public static void main(String[] args){

		String size = "M";

		switch(size){

			case "S":
				System.out.println("Small");
				break;

			case "M":
				System.out.println("Medium");
				break;

			case "L":
				System.out.println("Large");
				break;

			case "XL":
				System.out.println("Extra Large");
				break;

			case "2XL":
				System.out.println("Double Extra Large");
				break;

			case "3XL":
				System.out.println("Triple Extra Large");
				break;

			default:
				System.out.println("Enter a valid size (S,M,L,XL,2XL,3XL)");
			
		}

	}

}	
