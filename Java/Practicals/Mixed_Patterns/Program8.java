/*Q8 Write a program to reverse the given number.
Rows = 3
F E D
C B
A
Rows = 4
J I H G
F E D
C B
A*/
import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Row");
		int row=sc.nextInt();
		int num=(64+(row*(row+1))/2);
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print((char)num+" ");
				num--;
			}
			System.out.println();
		}
	}
}
