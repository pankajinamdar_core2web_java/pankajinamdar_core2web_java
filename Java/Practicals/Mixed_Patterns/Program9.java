/*Q9 Write a program to reverse the given number.
Rows = 4
1 2 3 4
C B A
1 2
A
Rows = 5
1 2 3 4 5
D C B A
1 2 3
B A
1*/
import java.util.*;
class Demo{
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Row:");
	int row=sc.nextInt();
	for(int i=1;i<=row;i++){
		int num=1;
		int num2=row+65-i;
		for(int j=row;j>=i;j--){
			if(i%2==1){
				System.out.print(num+" ");
				num++;
			}else{
				System.out.print((char)num2+" ");
				num2--;
			}
			//num++;
			//num2--;
		}
		System.out.println();
		}
	}
}
