/*Q4 Write a program to check whether the given number is composite or
not.
Row = 3
3
2 4
1 2 3
Rows = 4
4
3 6
2 4 6
1 2 3 4*/
import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Row:");
		int row=sc.nextInt();
		int num=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(num*j+" ");
			}
			num--;
			System.out.println();
		}
	
	}
}
