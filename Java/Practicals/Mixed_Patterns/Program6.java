/*Q6 Write a program to print the factorial of the number.
Rows = 3
c
3 2
c b a

Rows = 4
d
4 3
d c b
4 3 2 1*/
import java.util.*;
class Demo{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Row:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=96+row;
			int num2=row;
			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print((char)num+" ");
					num--;
				}else{
					System.out.print(num2+" ");
					num2--;
				}
			}
			System.out.println();
		}
	}
}
