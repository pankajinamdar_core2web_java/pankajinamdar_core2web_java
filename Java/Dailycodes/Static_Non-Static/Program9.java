class Demo{
        void methodFun(){
                System.out.println("In fun");
        }
        void methodRun(){
                System.out.println("In run");
        }
        void methodGun(){
                System.out.println("In gun:");
        }


        public static void main(String[] args){
                System.out.println("In main");
                Demo obj=new Demo();
	       	obj.methodRun();
                obj.methodFun();
                obj.methodGun();
        }
}
