class Demo{
        void fun(){
                System.out.println("In Fun");
        }
        static void Run(){
                System.out.println("In Run");
        }
        public static void main(String[] args){
                System.out.println("In Main");
                Demo obj=new Demo();
		obj.fun()
		Run();

        }
}
//Firstly memory gets allocate to static and main method in the the method area of the jvm and when we create a new object of class that memory gets allocate in the heap of jvm 
