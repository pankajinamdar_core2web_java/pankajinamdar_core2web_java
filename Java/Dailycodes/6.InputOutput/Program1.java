class Demo{
	void fun(){
		System.out.println("In Fun");
	}
	void Run(){
		System.out.println("In Run");
	}
	public static void main(String[] args){
		System.out.println("In Main");
		Demo.Run();
	
	}
}
//we cannot call non static method in static context directly.We can by the class name only when the outer method is static
