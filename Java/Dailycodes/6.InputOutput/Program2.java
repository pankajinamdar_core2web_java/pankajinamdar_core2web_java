class Demo{
        void fun(){
                System.out.println("In Fun");
        }
        static void Run(){
                System.out.println("In Run");
        }
        public static void main(String[] args){
                System.out.println("In Main");
                Demo.Run();

        }

}
//We call static method directly in main method with class name but it should have to be static first
