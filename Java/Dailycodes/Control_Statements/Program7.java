class SwitchDemo{
        public static void main(String [] args){
                char x='B';
                System.out.println("Before Switch");
        switch(x){
                case 'A':
                        System.out.println("A");

                case 65:
                     	System.out.println("65");
	     
                case 'B':
                        System.out.println("B");

                case 66:
			System.out.println("66");

        }
System.out.println("After Switch");
        }
  } //Duplicate case is not allowed in Switch here the ASCII value of A is 65 and B is 66 hence here duplication is getting created. It will give error :Duplicate case label
