class SwitchDemo{
	public static void main(String[] args){
		String friends="Shraddha";
		System.out.println("Before Switch");
	        switch(friends){
			case "Pankaj":
				System.out.print("Microsoft");  
			break;
			case "Shraddha":
				System.out.println("Google");
			break;
			default:
			        System.out.println("In default state");
		}
		System.out.println("After Switch");
	}
}

//while writing switch ,break should be written to stop the further case comparison.If we dont write break then will print the cases after the required case is matched.
