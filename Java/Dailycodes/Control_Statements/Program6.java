class SwitchDemo{
        public static void main(String [] args){
                char x='A';
                System.out.println("Before Switch");
        switch(x){
                case 'A':
                        System.out.println("Twenty");

                case 'B':
                        System.out.println("Twentyone");

        }
System.out.println("After Switch");
        }
} //it supports char datatype also ,if we dont write the Switch properly then it will ignore the cases but not the body of the case.It will directly print the body of the cases after the switch case is matched
